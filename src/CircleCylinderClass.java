import com.devcamp.models.Circle;

public class CircleCylinderClass{
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        System.out.println("Circle 1: ");
        System.out.println(circle1);

        System.out.println("------------------------------------------");

        Circle circle2 = new Circle(5);
        System.out.println("Circle 2: ");
        System.out.println(circle2);

        System.out.println("------------------------------------------");

        Circle circle3 = new Circle(7, "blue");
        System.out.println("Circle 3: ");
        System.out.println(circle3);

        System.out.println("------------------------------------------");

        Cylinder cylinder1 = new Cylinder();
        System.out.println("Cylinder 1: ");
        System.out.println(cylinder1);

        System.out.println("------------------------------------------");

        Cylinder cylinder2 = new Cylinder(2.5);
        System.out.println("Cylinder 2: ");
        System.out.println(cylinder2);

        System.out.println("------------------------------------------");

        Cylinder cylinder3 = new Cylinder(3.5, 1.5);
        System.out.println("Cylinder 3: ");
        System.out.println(cylinder3);

        System.out.println("------------------------------------------");

        Cylinder cylinder4  = new Cylinder(3.5,  "green", 1.5);
        System.out.println("Cylinder 4: ");
        System.out.println(cylinder4 );
    }
}
